# mc_rtc assignment

## About

This repo contains the result of an assignment for getting familiar with mc_rtc (https://jrl-umi3218.github.io/mc_rtc/index.html). The assignment is available at https://staff.aist.go.jp/rafael.cisneros/2021-rpd-ith_0009_practical_test.html.

## Requirements

Required operating system:
- Ubuntu 18.04 or 20.04

Required software:
- ros (melodic in Ubuntu version is 18.04 or noetic if Ubuntu version is 20.04)
- mc_rtc
- mc-rtc-ros
- python3
- rviz (if not installed with ros)

For instance, it was tested with Ubuntu 20.04, ROS noetic, and Python 3.8.10. 

## Installation

The program is written in Python. We assume there is a folder `$HOME/my_python_controllers` where you store your mc_rtc python controllers. If not, you should replace `my_python_controllers` by the `path/to/your/python/controllers/folder` in the installation commands. 

Given this information, the install can now be done as follow:

1. Go to `my_python_controllers` folder:
```
cd `my_python_controllers`
```

2. Clone the repo:
```
git clone https://framagit.org/selim-cm/mc_rtc-assignment
```

3. Go inside the cloned repo:
```
cd mc_rtc-assignment
```

4. Copy the configuration file to the path read by mc_rtc (should overwrite your existing file if any):
```
cp mc_rtc.yaml ~/.config/mc_rtc/mc_rtc.yaml
```

## Run the controller

1. In a first terminal, start a roscore:
```
roscore
```

2. In a second terminal, run the simulation (based on rviz):
```
roslaunch mc_rtc_ticker display.launch
```

3. In a third terminal, run the controller by specifying the path to our controller to mc_rtc_ticker: 
```
PYTHONPATH=$HOME/my_python_controllers:$PYTHONPATH rosrun mc_rtc_ticker mc_rtc_ticker
```

For more information about launching controllers with mc_rtc, see [this page](https://jrl-umi3218.github.io/mc_rtc/tutorials/introduction/running-a-controller.html).


## Results

The controller gives the following output in rviz:

![](demo/demo.gif)

A better quality video is available [here](demo/demo.mp4).

## Author and License
Copyright © 2022 Sélim Chefchaouni Moussaoui. MIT License.
