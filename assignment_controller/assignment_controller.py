#!/usr/bin/env python

"""AssignmentController.py: Controller file for the assignment."""

__author__      = "Sélim Chefchaouni Moussaoui"
__email__       = "selim.cm@mailo.com"
__copyright__   = "Copyright © 2022 Sélim Chefchaouni Moussaoui"
__license__     = "MIT"


import mc_control
import mc_rbdyn
import mc_rtc
import mc_tasks
import sva 
import math
import eigen
import time 
import numpy as np

class AssignmentController(mc_control.MCPythonController):
    """
    Class for the AssignmentController controller. The controller works with
    JVRC1 robot provided by mc_rtc. It executes the following 6-steps sequence 
    and repeats it until the controller is shut down:
    1. The left-hand moves to (0.5, 0.25, 1.1) with orientation (0.7, 0, 0.7, 0) 
      (w, x, y, z)
    2. The left-hand moves back to its initial position
    3. The right-hand moves to (0.5, -0.25, 1.1) with orientation (0.7, 0, 0.7, 0)
    4. The right-hand moves back to its inital position
    5. Both hands move to their respective specified position
    6. Both hands go back to their initial position
    While a single hand is moving the robot looks at the moving hand, when both 
    hands are moving the robot looks forward.
    """
    
    def __init__(self, rm, dt):
        """Constructor for AssignmentController"""

        # init EndEffectorTask tasks for hands
        self.efTaskLeftHand = mc_tasks.EndEffectorTask("l_wrist", self.robots(), 0, 
            stiffness=10.0, 
            weight=100.0
        )
        self.efTaskRightHand = mc_tasks.EndEffectorTask("r_wrist", self.robots(), 0, 
            stiffness=10.0, 
            weight=100.0
        )

        # init EndEffectorTask task for head
        self.efTaskHead = mc_tasks.EndEffectorTask("NECK_P_S", self.robots(), 0, 
            stiffness=10.0, 
            weight=100.0
        )

        # init qpsolver with contraints and tasks
        self.qpsolver.addConstraintSet(self.kinematicsConstraint)
        self.qpsolver.addConstraintSet(self.contactConstraint)
        self.qpsolver.addTask(self.postureTask) 
        self.qpsolver.addTask(self.efTaskLeftHand)
        self.qpsolver.addTask(self.efTaskRightHand)
        self.qpsolver.addTask(self.efTaskHead)

        # add contacts between the feets and the ground
        self.addContact(self.robot().name(), "ground", "LeftFoot", "AllGround")
        self.addContact(self.robot().name(), "ground", "RightFoot", "AllGround")

        # TODO: remove this as it is unused in current controller
        # find robot joints
        # for reminder, the list of all available joints for JVRC1 is:
        # ['R_HIP_P', 'R_HIP_R', 'R_HIP_Y', 'R_KNEE', 'R_ANKLE_R', 'R_ANKLE_P', 
        # 'L_HIP_P', 'L_HIP_R', 'L_HIP_Y', 'L_KNEE', 'L_ANKLE_R', 'L_ANKLE_P', 
        # 'WAIST_Y', 'WAIST_P', 'WAIST_R', 'NECK_Y', 'NECK_R', 'NECK_P', 
        # 'R_SHOULDER_P', 'R_SHOULDER_R', 'R_SHOULDER_Y', 'R_ELBOW_P', 
        # 'R_ELBOW_Y', 'R_WRIST_R', 'R_WRIST_Y', 'R_UTHUMB', 'R_LTHUMB', 
        # 'R_UINDEX', 'R_LINDEX', 'R_ULITTLE', 'R_LLITTLE', 'L_SHOULDER_P', 
        # 'L_SHOULDER_R', 'L_SHOULDER_Y', 'L_ELBOW_P', 'L_ELBOW_Y', 'L_WRIST_R', 
        # 'L_WRIST_Y', 'L_UTHUMB', 'L_LTHUMB', 'L_UINDEX', 'L_LINDEX', 
        # 'L_ULITTLE', 'L_LLITTLE']
        rm = mc_rbdyn.get_robot_module("JVRC1")
        list_joints_names = rm.ref_joint_order()
        list_joint_indexes = [i for i in range(1, len(list_joints_names)+1)]
        self.joints_dict = dict(zip(list_joints_names, list_joint_indexes))
        
        # threshold for considering a target is reached
        self.efStopThreshold = [0.05 for i in range(6)]

        # initial positions for hands
        self.initialPoseLeftHand = self.efTaskLeftHand.get_ef_pose()
        self.initialPoseRightHand = self.efTaskRightHand.get_ef_pose()

        # objective positions for hands
        self.desiredPoseLeftHand = sva.PTransformd(
            sva.RotY(-math.pi/2), # orientation (0.707107, 0, 0.707107, 0) (w, x, y, z)
            eigen.Vector3d(0.5, 0.25, 1.1) # position (0.5, 0.25, 1.1) (x, y, z)
        )
        self.desiredPoseRightHand = sva.PTransformd(
            sva.RotY(-math.pi/2), # idem
            eigen.Vector3d(0.5, -0.25, 1.1)
        )

        # initial orientation for head
        self.initialOrientationHead = self.efTaskHead.get_ef_pose()

        # objective orientations for head
        self.desiredOrientationHeadLeft = sva.PTransformd(
            eigen.Quaterniond(0.7, 0.0, -0.1, -0.25).toRotationMatrix(),
            self.initialOrientationHead.translation()
        )
        self.desiredOrientationHeadRight = sva.PTransformd(
            eigen.Quaterniond(0.7, 0.0, -0.1, 0.25).toRotationMatrix(),
            self.initialOrientationHead.translation()
        )

        # init timer for tasks
        # TODO: replace time conditions with position conditions
        self.t = time.time()

        # step of the controller (from 1 to 6)
        self.step = 1
        
        # run_callback first iteration
        self.runFirstIteration = True


    def run_callback(self):
        """Behaviour when the controller runs. See class description for the 
        steps."""

        if self.runFirstIteration == True:
            print("starting step 1")
            self.runFirstIteration = False
        
        # step 1: move the left hand to its desired position
        if (self.step==1):
            # set the objectives: the left hand should go to the desired 
            # position and the head should look to the hand
            self.efTaskLeftHand.set_ef_pose(self.desiredPoseLeftHand)
            self.efTaskHead.set_ef_pose(self.desiredOrientationHeadLeft)
            
            # after the position is reached, go to next step
            if (list(self.efTaskLeftHand.eval()) < self.efStopThreshold and 
                (time.time()-self.t)>5):
                self.step = 2
                print("starting step 2")

        # step 2: move the left hand to its initial position
        if (self.step==2):
            self.efTaskLeftHand.set_ef_pose(self.initialPoseLeftHand)
            self.efTaskHead.set_ef_pose(self.initialOrientationHead)
            if (list(self.efTaskLeftHand.eval()) < self.efStopThreshold and 
                (time.time()-self.t)>10):
                self.step = 3
                print("starting step 3")

        # step 3: move the right hand to its desired position
        if (self.step==3):
            self.efTaskRightHand.set_ef_pose(self.desiredPoseRightHand)
            self.efTaskHead.set_ef_pose(self.desiredOrientationHeadRight)
            if (list(self.efTaskRightHand.eval()) < self.efStopThreshold and 
                (time.time()-self.t)>15):
                self.step = 4
                print("starting step 4")

        # step 4: move the right hand to its initial position
        if (self.step==4):
            self.efTaskRightHand.set_ef_pose(self.initialPoseRightHand)
            self.efTaskHead.set_ef_pose(self.initialOrientationHead)
            if (list(self.efTaskRightHand.eval()) < self.efStopThreshold and 
                (time.time()-self.t)>20):
                self.step = 5
                print("starting step 5")

        # step 5: move both hand to their desired positions
        if (self.step==5):
            self.efTaskLeftHand.set_ef_pose(self.desiredPoseLeftHand)
            self.efTaskRightHand.set_ef_pose(self.desiredPoseRightHand)
            if (list(self.efTaskLeftHand.eval()) < self.efStopThreshold and
                list(self.efTaskRightHand.eval()) < self.efStopThreshold and 
                (time.time()-self.t)>25):
                self.step = 6
                print("starting step 6")
           
        # step 6: move both hand to their initial positions
        if (self.step==6):
            self.efTaskLeftHand.set_ef_pose(self.initialPoseLeftHand)
            self.efTaskRightHand.set_ef_pose(self.initialPoseRightHand)
            if (list(self.efTaskLeftHand.eval()) < self.efStopThreshold and
                list(self.efTaskRightHand.eval()) < self.efStopThreshold and 
                (time.time()-self.t)>30) :
                self.step = 1
                print("starting task 1")
                self.t = time.time()

        # run_callback should return true if the controller runs well
        return True



    def reset_callback(self, data):
        """Behaviour when the controller resets."""
        # reset tasks
        self.efTaskLeftHand.reset()
        self.efTaskRightHand.reset()


    @staticmethod
    def create(robot, dt):
        """Creates the controller with environment."""
        env = mc_rbdyn.get_robot_module("env", mc_rtc.MC_ENV_DESCRIPTION_PATH, "ground")
        print(env)
        return AssignmentController([robot,env], dt)